using UnityEngine;
using System.Collections;
using UnityEngine.AI;

[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Scripts/EnemyScripts/EnemyObject")]
public class EnemyObject : PoolObject, IKillable, IAttacker {
    #region spawn settings
    [Header("Spawn Settings")]
    [Tooltip("Only spawn every xth wave. x is determined by StartingWaveNumber.")]
    public bool ShowEveryXthWave = false;

    [Tooltip("How many extra of this enemy type show per every wave it spawns")]
    public int NumberIncrementPerWave = 2;

    [Tooltip("The first wave this enemy spawns")]
    public int StartingWaveNumber = 1;
    #endregion

    #region offense settings
    [Header("Offense Settings")]
    [Tooltip("Whether this enemy should attack the towers, or just run for it")]
    public bool AttacksTowers = false;

    [Tooltip("How far a tower must be from the enemy for it to attack")]
    public float EnemyCheckRadius = 1.0f;

    [Tooltip("How often the enemy attacks the tower it's targetting")]
    public float SecondsPerAttack = 1.0f;

    public float Damage = 15.0f;
    #endregion

    #region audio
    [Header("Audio")]
    public AudioClip DeathSound;
    public AudioClip AttackSound;
    #endregion

    #region general enemy settings
    [Header("General Enemy Settings")]
    public float Health = 100;
    public int MoneyValue = 10;
    #endregion

    public bool InCombat { private set; get; } = false;
    public bool IsDead { private set; get; } = false;
    public Transform EnemyTransform { private set; get; }
    private float _attackTimer;
    private float _startingHealth;
    private float _distanceToEnd;

    private IPlayerObject _currentTarget = null;

    private Animation _animation;
    private AudioSource _audioSource;
    private Vector3 _destination = Vector3.zero;
    private NavMeshAgent _navMeshAgent;

    private void Awake() {
        _attackTimer = SecondsPerAttack;
        _animation = GetComponent<Animation>();
        _audioSource = GetComponent<AudioSource>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _startingHealth = Health;
        EnemyTransform = transform;
    }

    private void Update() {
        if (IsDead || _destination == Vector3.zero) {
            return;
        }

        // If the end of the path has been reached, time to be disabled!
        if ((_distanceToEnd = Vector3.Distance(transform.position, _destination)) <= 1) {
            GameController.MainInstance.InitiateEnemyEscapeActions();
            DisablePoolObject();
            return;
        }

        // If the current target's dead or non-existing, no reason to attack it anymore
        CheckTargetStillValid();

        // Search for a nearby tower if this enemy can attack them
        if (AttacksTowers && !InCombat && _currentTarget == null) {
            SearchForTarget();
        }

        if (InCombat) {
            ApplyCombat();
        }
    }

    private void OnEnable() {
        EnablePoolObject(ResetPoolValues);
    }

    private void OnDisable() {
        GameController.MainInstance.Enemies.Remove(this);
    }

    private void OnTriggerEnter(Collider collider) {
        if (IsDead) {
            return;
        }

        // If the player builds a new tower right when a skeleton is attacking, this will
        // set the old target to null again if it's been removed.
        CheckTargetStillValid();

        // Ignore trigger enters on objects that aren't the current target
        if (_currentTarget != null && collider.gameObject != _currentTarget.PlayerGameObject) {
            return;
        }

        if (collider.CompareTag("Player") ||
            (collider.CompareTag("Tower") && AttacksTowers)) {
            AttackTarget(collider.gameObject);
            _navMeshAgent.SetDestination(EnemyTransform.position);
            _navMeshAgent.velocity = Vector3.zero;
        }
    }

    private void OnTriggerExit(Collider collider) {
        if (IsDead || !collider.gameObject || _currentTarget == null) {
            return;
        }

        if (collider.gameObject == _currentTarget.PlayerGameObject) {
            _navMeshAgent.SetDestination(_currentTarget.PlayerTransform.position);
        }
    }

    public void TakeDamage(float damage) {
        if (IsDead) {
            return;
        }

        if ((Health -= damage) <= 0) {
            Die();
        }
    }

    public void Die() {
        StaticGameMethods.PlaySound(_audioSource, DeathSound);

        IsDead = true;
        _navMeshAgent.enabled = false;
        _animation.Play("die");
        StartCoroutine("DestroyAfterDeath");

        GameController.MainInstance.AddMoney(MoneyValue);
        GameController.MainInstance.Enemies.Remove(this);
    }

    public void AttackTarget(GameObject target) {
        if (IsDead) {
            return;
        }

        var potentialTarget = target.GetComponent<IPlayerObject>();

        // Don't attack the dead
        if (potentialTarget != null && potentialTarget.IsDead) {
            return;
        }

        InCombat = true;
        _currentTarget = potentialTarget;
        transform.LookAt(target.transform.position);
        _animation.Play("waitingforbattle");

        if (potentialTarget != null && !potentialTarget.InCombat) {
            potentialTarget.AttackTarget(gameObject);
        }
    }

    private void ApplyCombat() {
        if (_currentTarget != null && _currentTarget.PlayerGameObject != null) {
            transform.LookAt(_currentTarget.PlayerTransform.position);

            if ((_attackTimer -= Time.deltaTime) <= 0) {
                _attackTimer = SecondsPerAttack;

                if (!_currentTarget.IsDead) {
                    _currentTarget.TakeDamage(Damage);

                    StaticGameMethods.PlaySound(_audioSource, AttackSound);
                    _animation.Play("attack");
                } else {
                    _currentTarget = null;
                }
            }
        }

        // Not an else since this could be set to null in the above if statement
        if (_currentTarget == null || _currentTarget.PlayerGameObject == null) {
            InCombat = false;
            _animation.Play("run");
            _navMeshAgent.SetDestination(_destination);
        }

        // Play the waiting animation in case an animation isn't already playing
        if (!_animation.isPlaying && InCombat) {
            _animation.Play("waitingforbattle");
        }
    }

    private void CheckTargetStillValid() {
        if (_currentTarget != null &&
            (_currentTarget.IsDead || _currentTarget.PlayerGameObject == null)) {
            _currentTarget = null;
        }

        if (_currentTarget == null) {
            InCombat = false;
            _animation.Play("run");
            _navMeshAgent.SetDestination(_destination);
        }
    }

    private IEnumerator DestroyAfterDeath() {
        yield return new WaitForSeconds(_animation.GetClip("die").length);
        DisablePoolObject();
    }

    private void DestroyObject() {
        DisablePoolObject();
    }

    private void SearchForTarget() {
        // No towers are built yet
        if (GameController.MainInstance.Towers.Count == 0) {
            return;
        }

        TowerObject closestTower = GetClosestTower();

        if (closestTower == null) {
            _navMeshAgent.SetDestination(_destination);
        } else {
            _navMeshAgent.SetDestination(closestTower.transform.position);
            _currentTarget = closestTower as IPlayerObject;
        }
    }

    private TowerObject GetClosestTower() {
        GameObject closestTower = null;

        foreach (var tower in GameController.MainInstance.Towers) {
            // If the tower is alive, store a path from the tower's to the end of the level
            if (!tower.IsDead) {
                float towerToEnd = Vector3.Distance(tower.PlayerTransform.position, _destination) - EnemyCheckRadius;

                // The tower is closer to the end of the level, so it becomes a target for the enemy
                if (towerToEnd <= _distanceToEnd) {
                    // Set it immediately, otherwise check if it's closest to the current tower target
                    if (closestTower == null) {
                        closestTower = tower.gameObject;
                    } else {
                        closestTower = gameObject.GetCloserObject(tower.gameObject, closestTower.gameObject);
                    }
                }
            }
        }

        // Don't want to call GetComponent too many times, so do it once after the full search has been complete
        return closestTower?.GetComponent<TowerObject>();
    }

    private void ResetPoolValues() {
        Health = _startingHealth;
        IsDead = false;
        InCombat = false;

        _navMeshAgent.enabled = true;
        _destination = GameController.MainInstance.EndPosition;
        _currentTarget = null;

        _animation.Play("run");
        _navMeshAgent.SetDestination(_destination);

        GameController.MainInstance.Enemies.Add(this);
    }
}
