﻿using System.Collections.Generic;
using UnityEngine;

public static class GameExtensions {

    /// <summary>
    /// Since unity only allows you to compare 1 tag to an object, this allows to compare multiple tags. Acts the same
    /// as CompareTag() but with an array of strings
    /// </summary>
    /// <param name="mainObject">The initial object</param>
    /// <param name="tags">The tags to compare this object's tag to</param>
    /// <returns></returns>
    public static bool CompareTags(this GameObject mainObject, params string[] tags) {
        foreach (string tag in tags) {
            if (mainObject.CompareTag(tag)) {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Returns the closest object to this object.
    /// </summary>
    /// <param name="mainObject">The main object to compare others to</param>
    /// <param name="otherObjects">A list of objects to find closest to this object</param>
    /// <returns>The closest object found in otherObjects</returns>
    public static GameObject GetCloserObject(this GameObject mainObject, params GameObject[] otherObjects) {
        if (otherObjects.Length == 0) {
            return mainObject;
        }

        GameObject closestObject = otherObjects[0];

        foreach (var obj in otherObjects) {
            if (Vector3.Distance(mainObject.transform.position, obj.transform.position) <
                Vector3.Distance(mainObject.transform.position, closestObject.transform.position)) {
                closestObject = obj;
            }
        }

        return closestObject;
    }

    /// <summary>
    /// An extension for IList objects, using Fisher-Yates shuffle algorithm
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list">The list to shuffle</param>
    public static void Shuffle<T>(this IList<T> list) {
        int n = list.Count;
        while (n > 1) {
            n--;
            int k = Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
