using UnityEngine;

public static class StaticGameMethods {
    public static void PlaySound(AudioSource source, AudioClip sound) {
        if (StaticGameValues.SoundOn) {
            source.PlayOneShot(sound);
        }
    }

    public static void PlayMusic(AudioSource source, AudioClip sound = null) {
        if (StaticGameValues.MusicOn) {
            if (sound != null) {
                source.PlayOneShot(sound);
            } else {
                source.Play();
            }
        }
    }
}
