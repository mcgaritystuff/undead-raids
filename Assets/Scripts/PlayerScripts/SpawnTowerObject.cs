using UnityEngine;
using System.Collections.Generic;

[AddComponentMenu("Scripts/PlayerScripts/SpawnTowerObject")]
public class SpawnTowerObject : TowerObject {
    [Header("Paladin Spawning Settings")]
    public float SecondsPerSpawn = 5.0f;
    public int MaxSpawnCount = 5;
    public PoolObject PlayerObjectPrefab;
    public Transform SpawnLocation;

    private float _secondsPerSpawnReset;
    private List<Paladin> _paladins;

    protected override void Start() {
        _secondsPerSpawnReset = SecondsPerSpawn;
        _paladins = new List<Paladin>(MaxSpawnCount);
        base.Start();
    }

    protected override void Update() {
        if (IsDead) {
            return;
        }

        if (_paladins.Count < MaxSpawnCount && (SecondsPerSpawn -= Time.deltaTime) <= 0) {
            SecondsPerSpawn = _secondsPerSpawnReset;
            Paladin newPaladin = ObjectPoolManager.Instance.RetrieveNewlyActiveObject(PlayerObjectPrefab, SpawnLocation.position, PlayerTransform.rotation).GetComponent<Paladin>();
            newPaladin.SetSpawnTower(this);
            _paladins.Add(newPaladin);
        }

        base.Update();
    }

    public void RemovePaladin(Paladin paladin) {
        _paladins.Remove(paladin);
    }
}
