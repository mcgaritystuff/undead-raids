using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Scripts/PlayerScripts/TowerObject")]
public class TowerObject : MonoBehaviour, IPlayerObject {
    [Header("Attack Settings")]
    public float ArrowDamage = 5.0f;
    public float ArrowSpeed = 4.0f;
    public float AttackRange = 2.0f;
    public float SecondsPerAttack = 1.0f;
    public AudioClip AttackSound;
    public Arrow Missile;

    [Header("General Tower Settings")]
    public float MaxHealth = 100.0f;
    [Tooltip("Where the arrow should start on the tower in the y-axis")]
    public float ArrowStartingYPosition = 1.5f;
    public int Value = 50;
    [Tooltip("This is the tower name for the UI Purchase Menu")]
    public string TowerName = "NA";

    [Header("Upgrade Incrementals")]
    public float SecondsPerAttackPerUpgrade = 0.02f;
    public int ArrowDamagePerUpgrade = 1;
    public int HealthPerUpgrade = 10;
    public int ValuePerUpgrade = 50;

    public bool InCombat { private set; get; }
    public bool IsDead { private set; get; }
    public float Health { private set; get; }
    public int HealPrice { get { return (int)MaxHealth / 4; } }
    public int Level { set; get; }
    public GameObject PlayerGameObject { private set; get; }
    public Transform PlayerTransform { private set; get; }
    
    private float _attackTimer;
    private AudioSource _audioSource;
    private EnemyObject _currentTarget;

    protected virtual void Start() {
        Health = MaxHealth;
        InCombat = false;
        PlayerTransform = transform;
        PlayerGameObject = gameObject;

        _audioSource = GetComponent<AudioSource>();

        _attackTimer = SecondsPerAttack;

        if (TowerName != "NA") {
            GameController.MainInstance.Towers.Add(this);
        }
    }

    protected virtual void Update() {
        if (IsDead) {
            return;
        }

        CheckTargetStillValid();

        if (!InCombat && _currentTarget == null) {
            SearchForTarget();
        }

        if (_currentTarget != null && (_attackTimer -= Time.deltaTime) <= 0) {
            AttackTarget(_currentTarget.gameObject);
            _attackTimer = SecondsPerAttack;
        }
    }

    public void AttackTarget(GameObject target) {
        if (IsDead || Missile == null || _attackTimer > 0) {
            return;
        }

        if (_currentTarget.IsDead ||
            !InRangeOfAttack(_currentTarget)) {
            _currentTarget = null;
            return;
        }

        Vector3 startPosition = PlayerTransform.position + new Vector3(0, ArrowStartingYPosition, 0);
        Vector3 endPosition = _currentTarget.EnemyTransform.position + _currentTarget.transform.up;

        if (!_currentTarget.InCombat) {
            endPosition += _currentTarget.EnemyTransform.forward;
        }

        GameObject arrow = ObjectPoolManager.Instance.RetrieveNewlyActiveObject(
                                                                Missile, 
                                                                startPosition, 
                                                                Quaternion.LookRotation((endPosition - startPosition).normalized)
                                                             );

        if (arrow != null) {
            StaticGameMethods.PlaySound(_audioSource, AttackSound);
            arrow.GetComponent<Arrow>().FireArrow(ArrowDamage, ArrowSpeed);
        }
    }

    public void Die() {
        IsDead = true;
        PlayerTransform.GetComponent<BoxCollider>().enabled = false;
    }

    public void Heal(float amount) {
        Health += amount;

        if (Health > MaxHealth) {
            Health = MaxHealth;
        }

        if (IsDead) {
            IsDead = false;
            PlayerTransform.GetComponent<BoxCollider>().enabled = true;
        }
    }

    public TowerObject LevelUp(TowerObject newModel) {
        TowerObject newTower;

        // Upgrade the current tower, otherwise build the new one from scratch
        if (TowerName == newModel.TowerName) {
            newTower = this;
            MaxHealth += HealthPerUpgrade;
            Level = Level + 1;
            ArrowDamage += ArrowDamagePerUpgrade;
            SecondsPerAttack -= SecondsPerAttackPerUpgrade;
            Value += ValuePerUpgrade;

            Heal(MaxHealth);
        } else {
            GameController.MainInstance.Towers.Remove(this);
            Destroy(PlayerGameObject);
            newTower = Instantiate(newModel, PlayerTransform.position, PlayerTransform.rotation);
            newTower.Level = 1;
        }

        return newTower;
    }

    public void TakeDamage(float damage) {
        if ((Health -= damage) <= 0) {
            Die();
        }
    }

    private void CheckTargetStillValid() {
        if (_currentTarget != null && _currentTarget.IsDead) {
            _currentTarget = null;
        }

        if (_currentTarget == null) {
            InCombat = false;
        }
    }

    private void SearchForTarget() {
        EnemyObject closestEnemy = GetClosestEnemy();

        if (closestEnemy != null) {
            _currentTarget = closestEnemy;
        }
    }

    private EnemyObject GetClosestEnemy() {
        GameObject closestEnemy = null;

        foreach (var enemy in GameController.MainInstance.Enemies) {
            if (!enemy.IsDead && InRangeOfAttack(enemy)) {
                float enemyDistanceToStart = Vector3.Distance(enemy.transform.position, GameController.MainInstance.StartPositionTransform.position);

                if (closestEnemy == null) {
                    closestEnemy = enemy.gameObject;
                } else {
                    closestEnemy = gameObject.GetCloserObject(enemy.gameObject, closestEnemy.gameObject);
                }
            }
        }

        // Don't want to call GetComponent too many times, so do it once after the full search has been complete
        return closestEnemy?.GetComponent<EnemyObject>();
    }

    private bool InRangeOfAttack(EnemyObject enemy) {
        if (Vector3.Distance(enemy.EnemyTransform.position, PlayerTransform.position) < AttackRange) {
            return true;
        }

        return false;
    }
}
