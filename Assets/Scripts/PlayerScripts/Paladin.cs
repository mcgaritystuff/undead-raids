using UnityEngine;
using System.Collections;
using UnityEngine.AI;

[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Scripts/PlayerScripts/Paladin")]
public class Paladin : PoolObject, IPlayerObject {
    [Header("Attack Values")]
    public float SecondsPerAttack = 1.0f;
    public float Damage = 15.0f;
    public float DestroyAfterDieTime = 2.5f;
    public float ViewRange = 3.0f;

    [Header("Audio Clips")]
    public AudioClip DeathSound;
    public AudioClip AttackSound;

    [Header("Miscellaneous")]
    public float Health = 100;

    public bool InCombat { private set; get; }
    public bool IsDead { private set; get; }
    public GameObject PlayerGameObject { private set; get; }
    public Transform PlayerTransform { private set; get; }

    private float _attackTimer;
    private float _startingHealth;
    private Animator _animator;
    private AudioSource _audioSource;
    private EnemyObject _currentTarget;
    private NavMeshAgent _navMeshAgent;
    private SpawnTowerObject _respectiveSpawnTower;

    private static readonly float _enemyStartDistanceBuffer = 1.0f;

    private void Awake() {
        _attackTimer = SecondsPerAttack;
        _startingHealth = Health;

        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
        _navMeshAgent = GetComponent<NavMeshAgent>();

        PlayerTransform = transform;
        PlayerGameObject = gameObject;

        //Get "comfortable" so it's not on top of other objects
        _navMeshAgent.SetDestination(PlayerTransform.position + PlayerTransform.forward);
    }

    private void OnTriggerEnter(Collider collider) {
        if (IsDead) {
            return;
        }

        if (collider.CompareTag("Enemy")) {
            AttackTarget(collider.gameObject);

            _animator.SetBool("Walking", false);
            _navMeshAgent.SetDestination(PlayerTransform.position);
            _navMeshAgent.velocity = Vector3.zero;
        }
    }

    private void Update() {
        if (IsDead) {
            return;
        }

        CheckTargetStillValid();

        if (!InCombat && _currentTarget == null) {
            SearchForTarget();
        }

        if (InCombat) {
            ApplyCombat();
        }
    }

    private void OnEnable() {
        EnablePoolObject(ResetPoolValues);
    }

    public void TakeDamage(float damage) {
        if ((Health -= damage) <= 0) {
            Die();
        }
    }

    public void Die() {
        StaticGameMethods.PlaySound(_audioSource, DeathSound);
        IsDead = true;

        _navMeshAgent.enabled = false;
        _animator.SetBool("Attacking", false);
        _animator.SetBool("Walking", false);
        _animator.SetBool("Die", true);
        StartCoroutine("DestroyAfterDeath");
    }

    public void AttackTarget(GameObject target) {
        if (IsDead) {
            return;
        }

        _currentTarget = target.GetComponent<EnemyObject>();

        if (_currentTarget == null || _currentTarget.IsDead) {
            return;
        }

        InCombat = true;
        _navMeshAgent.SetDestination(_currentTarget.EnemyTransform.position);
        _animator.SetBool("Attacking", true);

        if (!_currentTarget.InCombat) {
            _currentTarget.AttackTarget(gameObject);
        }
    }

    public void SetSpawnTower(SpawnTowerObject spawnTower) {
        _respectiveSpawnTower = spawnTower;
    }

    private void ApplyCombat() {
        if (_currentTarget != null) {
            transform.LookAt(_currentTarget.transform.position);

            if (Vector3.Distance(PlayerTransform.position, _currentTarget.transform.position) > ViewRange) {
                _animator.SetBool("Walking", true);
                _animator.SetBool("Attacking", false);
            }

            if ((_attackTimer -= Time.deltaTime) <= 0) {
                _attackTimer = SecondsPerAttack;
                var enemy = _currentTarget.GetComponent<EnemyObject>();

                if (enemy != null && !enemy.IsDead) {
                    enemy.TakeDamage(Damage);

                    StaticGameMethods.PlaySound(_audioSource, AttackSound);
                } else {
                    _currentTarget = null;
                }
            }
        }

        // Not an else since this could be set to null in the above if statement
        if (_currentTarget == null) {
            InCombat = false;
            _animator.SetBool("Walking", false);
            _animator.SetBool("Attacking", false);
        }
    }

    private void CheckTargetStillValid() {
        if (_currentTarget != null && _currentTarget.IsDead) {
            _currentTarget = null;
        }

        if (_currentTarget == null) {
            InCombat = false;
            _navMeshAgent.SetDestination(PlayerTransform.position);
            _animator.SetBool("Attacking", false);
            _animator.SetBool("Walking", false);
            _navMeshAgent.velocity = Vector3.zero;
        }
    }

    private void SearchForTarget() {
        if (GameController.MainInstance.Enemies.Count == 0) {
            return;
        }

        EnemyObject closestEnemy = GetClosestEnemy();

        if (closestEnemy != null) {
            _animator.SetBool("Walking", true);
            _animator.SetBool("Attacking", false);
            _currentTarget = closestEnemy;
            _navMeshAgent.SetDestination(closestEnemy.transform.position);
        }
    }

    private EnemyObject GetClosestEnemy() {
        GameObject closestEnemy = null;

        foreach (var enemy in GameController.MainInstance.Enemies) {
            if (!enemy.IsDead && InRangeOfAttack(enemy)) {
                float enemyDistanceToStart = Vector3.Distance(enemy.transform.position, GameController.MainInstance.StartPositionTransform.position);

                if (enemyDistanceToStart > _enemyStartDistanceBuffer) {
                    if (closestEnemy == null) {
                        closestEnemy = enemy.gameObject;
                    } else {
                        closestEnemy = gameObject.GetCloserObject(enemy.gameObject, closestEnemy.gameObject);
                    }
                }
            }
        }

        // Don't want to call GetComponent too many times, so do it once after the full search has been complete
        return closestEnemy?.GetComponent<EnemyObject>();
    }

    private bool InRangeOfAttack(EnemyObject enemy) {
        if (Vector3.Distance(enemy.gameObject.transform.position, PlayerTransform.position) < ViewRange) {
            return true;
        }

        return false;
    }

    private IEnumerator DestroyAfterDeath() {
        yield return new WaitForSeconds(DestroyAfterDieTime);
        _respectiveSpawnTower.RemovePaladin(this);
        DisablePoolObject();
    }

    private void ResetPoolValues() {
        _attackTimer = SecondsPerAttack;
        _animator.SetBool("Walking", false);
        _animator.SetBool("Attacking", false);
        _currentTarget = null;
        _navMeshAgent.enabled = true;
        _respectiveSpawnTower = null;

        Health = _startingHealth;
        InCombat = false;
        IsDead = false;
    }
}
