using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[AddComponentMenu("Scripts/MenuStuff/MenuButton")]
public class MenuButton : MonoBehaviour
{
    public delegate void ActivateButton();    

    public bool IsActive = false;
    public bool LastSelectedButton = false;
    public ActivateButton ButtonActivate;
    public GameObject Area;
    public GameObject MenuCharacterDestinationNode;
    public List<ParticleSystem> BorderParticleSystems;

    private void Update()
    {
        if (IsActive)
        {
            var inactiveSystems = BorderParticleSystems.Where(ps => !ps.gameObject.activeInHierarchy);
            foreach (var system in inactiveSystems)
                system.gameObject.SetActive(true);

            if (Input.GetButtonDown("LeftClick"))
            {
                RaycastHit hit;

                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                    if (hit.collider.gameObject == Area.gameObject && ButtonActivate != null)
                        ButtonActivate();
            }
        }
        else
        {
            var inactiveSystems = BorderParticleSystems.Where(ps => ps.gameObject.activeInHierarchy);

            foreach (var system in inactiveSystems)
                system.gameObject.SetActive(false);
        }        
    }
}
