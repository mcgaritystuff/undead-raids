using UnityEngine;
using UnityEngine.AI;

[AddComponentMenu("Scripts/MenuStuff/MenuCharacter")]
public class MenuCharacter : MonoBehaviour 
{
    private Animation _animation;
    private NavMeshAgent _navMeshAgent;

	void Start () 
    {
        _animation = GetComponent<Animation>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
	}
	
	void Update () 
    {
        if (_navMeshAgent.remainingDistance > 0)
            _animation.Play("run");
        else
            _animation.Play("idle");
	}
}
