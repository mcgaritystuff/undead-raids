using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Scripts/MenuStuff/OptionsMenu")]
public class OptionsMenu : MonoBehaviour 
{
    public Toggle MusicOnToggle;
    public Toggle SoundOnToggle;

    private void Awake()
    {
        MusicOnToggle.onValueChanged.AddListener(musicOn => StaticGameValues.MusicOn = musicOn);
        SoundOnToggle.onValueChanged.AddListener(soundOn => StaticGameValues.SoundOn = soundOn);
    }
}
