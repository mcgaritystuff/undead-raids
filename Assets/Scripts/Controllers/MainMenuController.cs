using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

[AddComponentMenu("Scripts/Controllers/MainMenuController")]
public class MainMenuController : MonoBehaviour {
    [Header("Buttons")]
    public MenuButton NewGameButton;
    public MenuButton OptionsButton;
    public MenuButton InstructionsButton;
    public MenuButton ExitGameButton;

    [Header("Canvases")]
    public GameObject InstructionsCanvas;
    public GameObject OptionsCanvas;

    [Header("Miscellaneous")]
    public NavMeshAgent MenuCharacter;
    public string CancelInputAxis = "Cancel";

    private AudioSource _audioSource;
    private MenuButton[] _menuButtons;

    private void Start() {
        NewGameButton.ButtonActivate = (() => SceneManager.LoadScene("Game"));
        ExitGameButton.ButtonActivate = (() => Application.Quit());

        OptionsButton.ButtonActivate = (OpenOptionsMenu);
        InstructionsButton.ButtonActivate = (OpenInstructionsMenu);

        _menuButtons = new MenuButton[4] {
            NewGameButton,
            OptionsButton,
            InstructionsButton,
            ExitGameButton
        };

        _audioSource = GetComponent<AudioSource>();
        StaticGameMethods.PlayMusic(_audioSource);
    }

    private void Update() {
        // Store which button the mouse is pointing at right now
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)) {
            // Cycle through the buttons to see which ones should be active/inactive
            foreach (var button in _menuButtons) {
                if (button == null || button.Area == null) {
                    continue;
                }

                // Activate the button that's currently being hovered over
                if (hit.collider.gameObject == button.Area.gameObject) {
                    button.IsActive = true;
                    button.LastSelectedButton = true;
                    MenuCharacter.SetDestination(button.MenuCharacterDestinationNode.transform.position);
                } else {
                    button.IsActive = false;
                    button.LastSelectedButton = false;
                }
            }
        }

        // NOTE: Probably better as an Action in future games/updates
        if (_audioSource.isPlaying && !StaticGameValues.MusicOn) {
            _audioSource.Stop();
        } else if (!_audioSource.isPlaying) {
            StaticGameMethods.PlayMusic(_audioSource);
        }

        if (Input.GetButton(CancelInputAxis)) {
            OptionsCanvas.SetActive(false);
            InstructionsCanvas.SetActive(false);
        }
    }

    private void OpenOptionsMenu() {
        OptionsCanvas.SetActive(!OptionsCanvas.activeInHierarchy);
        InstructionsCanvas.SetActive(false);
    }

    private void OpenInstructionsMenu() {
        InstructionsCanvas.SetActive(!InstructionsCanvas.activeInHierarchy);
        OptionsCanvas.SetActive(false);
    }
}
