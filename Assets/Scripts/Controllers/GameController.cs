using UnityEngine;
using System.Collections.Generic;

[AddComponentMenu("Scripts/Controllers/GameController")]
public class GameController : MonoBehaviour {
    public static GameController MainInstance;

    public int EnemiesEscaped { private set; get; } = 0;
    public int WaveNumber { private set; get; } = 0; // StartNewWave() will increment this to 1 at the start

    public Vector3 EndPosition { private set; get; }
    public List<EnemyObject> Enemies { private set; get; }
    public List<TowerObject> Towers { private set; get; }

    public System.Action InitiateEnemyEscapeActions;
    public System.Action InitiateNewWave;
    public System.Action CurrencyChanged;

    #region spawning stuff
    [Header("Spawning Stuff")]
    [Tooltip("For every wave, lower the maximum time to spawn by this many seconds (until it reaches min)")]
    [Range(0f, 1f)]
    public float SpawnTimeDecrease = 0.1f;

    [Tooltip("The maximum amount of time that a spawn can happen")]
    public float MaxTimePerSpawn = 3.0f;

    [Tooltip("The minimum amount of time that a spawn can happen")]
    public float MinTimePerSpawn = 0.5f;

    public List<EnemyObject> EnemyTypes = new List<EnemyObject>();
    #endregion

    #region audio clips
    [Header("Audio Clips")]
    public AudioClip GameMusic;
    public AudioClip GameOverSound;
    public AudioClip HitEndSound;
    public AudioClip NewWaveSound;
    #endregion

    #region pathing
    [Header("Pathing")]
    public Transform EndPositionTransform;
    public Transform StartPositionTransform;
    #endregion

    #region canvases
    [Header("Canvases")]
    public GameObject GameOverCanvas;
    public GameObject PurchaseMenuCanvas;
    #endregion

    #region general game settings
    [Header("General Game Settings")]
    [Tooltip("Number of escapes that will result in a game over")]
    public int MaxEscapes = 10;
    [Tooltip("The amount of money the player starts with")]
    public int Money = 100;
    #endregion

    private AudioSource _audioSource;
    private float _spawnTimer;

    // This is the list to manage the spawning of enemies. Each int represents the index in EnemyTypes
    private int[] _spawnList = new int[0];
    // This is the position in the spawn list we are at while spawning new enemies from it. Saves from removing/adding to
    // a List<> instead.
    private int _spawnListPosition = 0;

    private void Awake() {
        MainInstance = this;

        Enemies = new List<EnemyObject>();
        Towers = new List<TowerObject>();
        EndPosition = EndPositionTransform.position;
    }

    private void Start() {
        // Just set the initial enemy spawn time
        _spawnTimer = Random.Range(MinTimePerSpawn, MaxTimePerSpawn);
        _audioSource = GetComponent<AudioSource>();

        StaticGameMethods.PlayMusic(_audioSource);

        InitiateEnemyEscapeActions += IncrementEnemyEscape;
        InitiateNewWave += StartNewWave;
    }

    private void Update() {
        UpdateWave();

        if (Enemies.Count == 0 && _spawnListPosition >= _spawnList.Length) {
            InitiateNewWave();
        }

        if (Input.GetButton("Cancel")) {
            PurchaseMenuCanvas.SetActive(false);
        }
    }

    /// <summary>
    /// Add one escape to the enemy counter while playing a sound
    /// </summary>
    private void IncrementEnemyEscape() {
        if (GameOverCanvas.activeInHierarchy) {
            return;
        }

        StaticGameMethods.PlaySound(_audioSource, HitEndSound);

        if (++EnemiesEscaped >= MaxEscapes) {
            GameOver();
        }
    }

    /// <summary>
    /// Change the amount money the player has by adding (or removing) a given amount
    /// </summary>
    /// <param name="amount">The amount to add/deduct</param>
    /// <returns>True if successful, false if not (Due to not enough funds).</returns>
    public bool AddMoney(int amount) {
        if (Money + amount >= 0) {
            Money += amount;
            CurrencyChanged();
            return true;
        } else {
            return false;
        }

    }

    private void GameOver() {
        StaticGameMethods.PlaySound(_audioSource, GameOverSound);
        GameOverCanvas.SetActive(true);
    }

    private void StartNewWave() {
        WaveNumber++;
        StaticGameMethods.PlaySound(_audioSource, NewWaveSound);

        // Decrease max time of spawn to this wave number. Short circuit if we've hit our minimum amount of time per spawn
        // BUT Don't let the max time go below the min time
        if (MaxTimePerSpawn > MinTimePerSpawn &&
            (MaxTimePerSpawn -= SpawnTimeDecrease) < MinTimePerSpawn) {
            MaxTimePerSpawn = MinTimePerSpawn;
        }

        int[,] enemiesInSpawnList = SetSpawnListSize();

        // Setup the _spawnList values to the correct corresponding EnemyType index
        for (int i = 0; i < enemiesInSpawnList.Length; i++) {
            for (int j = 0; j < enemiesInSpawnList[i, 0]; j++) {
                _spawnList[j * i] = i;
            }
        }

        // Shuffle the enemy indices around the spawn list for random spawning, 
        // and restart the index reference position to 0
        _spawnList.Shuffle();
        _spawnListPosition = 0;
    }

    /// <summary>
    /// Sets the size of the spawn list size to the number of enemies to this wave
    /// </summary>
    /// <returns>A double array matching the format of [EnemyType index, number of enemies]</returns>
    private int[,] SetSpawnListSize() {
        //Set enemies in the list as numbers representing the position of EnemyTypes list.
        int[,] enemiesInSpawnList = new int[EnemyTypes.Count, 1];
        int totalEnemyCount = 0;

        // Get the enemy counts for each enemy on this wave. Also accumulate the total number of enemies
        // so we can set the spawnList array to the right size
        for (int i = 0; i < EnemyTypes.Count; i++) {
            EnemyObject enemyType = EnemyTypes[i];

            if (enemyType.StartingWaveNumber <= WaveNumber) {
                int enemyCount = 0;

                if (enemyType.ShowEveryXthWave && WaveNumber % enemyType.StartingWaveNumber == 0) {
                    enemyCount = enemyType.NumberIncrementPerWave * (WaveNumber / enemyType.StartingWaveNumber);
                } else if (!enemyType.ShowEveryXthWave) {
                    enemyCount = enemyType.NumberIncrementPerWave * (WaveNumber - enemyType.StartingWaveNumber + 1);
                }

                enemiesInSpawnList[i, 0] = enemyCount;
                totalEnemyCount += enemyCount;
            }
        }

        // Setup the total spawn list count to the right enemy count
        _spawnList = new int[totalEnemyCount];

        return enemiesInSpawnList;
    }

    private void UpdateWave() {
        if (_spawnListPosition >= _spawnList.Length) {
            return;
        }

        if ((_spawnTimer -= Time.deltaTime) <= 0) {
            _spawnTimer = Random.Range(MinTimePerSpawn, MaxTimePerSpawn);
            CreateEnemy();
        }
    }

    private void CreateEnemy() {
        ObjectPoolManager.Instance.RetrieveNewlyActiveObject(
                                        EnemyTypes[_spawnList[_spawnListPosition++]],
                                        StartPositionTransform.position,
                                        StartPositionTransform.rotation
                                   );
    }
}