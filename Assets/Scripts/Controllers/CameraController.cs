using UnityEngine;
using Logger = LoggingUtils.Logger;

[AddComponentMenu("Scripts/Controllers/CameraController")]
public class CameraController : MonoBehaviour {
    #region public enums and fields

    public enum StartingXPosition {
        Left = -1,
        Center,
        Right
    };

    public enum StartingYPosition {
        Bottom = -1,
        Center,
        Top
    }

    [Header("Positioning")]
    [Tooltip("How high the camera should be on the y-axis by default")]
    public float DefaultHeight = 10;

    [Tooltip("Where the camera should start above the terrain along the X-axis")]
    public StartingXPosition InitialXPosition = StartingXPosition.Right;

    [Tooltip("Where the camera should start above the terrain along the Y-axis (z-axis in worldview)")]
    public StartingYPosition InitialYPosition = StartingYPosition.Bottom;

    [Header("Controls")]
    [Tooltip("The input for moving left/right")]
    public string PlayerHorizontalButton = "Horizontal";

    [Tooltip("The input for moving up/down")]
    public string PlayerVerticalButton = "Vertical";

    [Header("Miscellaneous")]
    [Tooltip("The terrain that the camera is hovering over")]
    public Terrain Terrain;
    #endregion

    private Vector3 bottomLeftCameraCorner, topRightCameraCorner;

    private void Start() {
        if (Terrain == null) {
            Logger.Error("Terrain cannot be null for the camera.");
        }

        SetStartPosition();
    }

    private void Update() {
        // Get the current camera's corners in the world view
        bottomLeftCameraCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, DefaultHeight));
        topRightCameraCorner = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, DefaultHeight));

        // Check movement input values without smoothing
        float HorizontalMovement = Input.GetAxisRaw(PlayerHorizontalButton);
        float VerticalMovement = Input.GetAxisRaw(PlayerVerticalButton);

        #region check camera-to-terrain constraints

        if (topRightCameraCorner.x + HorizontalMovement > Terrain.terrainData.size.x ||
            bottomLeftCameraCorner.x + HorizontalMovement < 0) {
            HorizontalMovement = 0;
        }

        if (topRightCameraCorner.z + VerticalMovement > Terrain.terrainData.size.z ||
            bottomLeftCameraCorner.z + VerticalMovement < 0) {
            VerticalMovement = 0;
        }

        #endregion

        Camera.main.transform.Translate(new Vector3(
            HorizontalMovement,
            VerticalMovement,
            0
        ));
    }

    /// <summary>
    /// Places the camera in the desired corners of the terrain
    /// </summary>
    private void SetStartPosition() {
        // Start by putting the camera in the dead center of the Terrain. This is so we get proper calculations of placement
        Camera.main.transform.position = new Vector3(
            Terrain.transform.position.x + Terrain.terrainData.size.x / 2,
            DefaultHeight,
            Terrain.transform.position.z + Terrain.terrainData.size.z / 2
        );

        bottomLeftCameraCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, DefaultHeight));
        topRightCameraCorner = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, DefaultHeight));

        // Using the bottom-left corner of the Terrain as a reference, move the Camera
        // to the desired corner of the terrain
        Camera.main.transform.position = new Vector3(
            Terrain.terrainData.size.x / 2 + ((float)InitialXPosition * bottomLeftCameraCorner.x),
            DefaultHeight,
            Terrain.terrainData.size.z / 2 + ((float)InitialYPosition * bottomLeftCameraCorner.z)
        );
    }
}
