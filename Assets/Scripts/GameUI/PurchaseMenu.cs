using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text;

[AddComponentMenu("Scripts/GameUI/PurchaseMenu")]
public class PurchaseMenu : MonoBehaviour {

    public AudioClip PurchaseSound;

    [Header("Game Objects")]
    public List<TowerObject> RespectivePurchasableItemScripts = new List<TowerObject>();

    [Header("UI Elements")]
    public Button PurchaseButton;
    public Button ExitMenuButton;
    [Tooltip("The buttons that purchase the respective tower objects. Place in the same order as the towers scripts.")]
    public List<Toggle> PurchaseableItemButtons = new List<Toggle>();
    public Text InformationText;
    public Text PurchaseText;

    #region private fields
    private int _previousIncome;
    private string _infoTextFormat;
    private AudioSource _audioSource;
    private Dictionary<Toggle, TowerObject> _towerToggles = new Dictionary<Toggle, TowerObject>();
    private TowerObject _newTowerObject;
    private TowerObject _towerToUpgrade;
    private TowerObject _selectedTower;
    private TowerUIScript _towerToUpgradeUI;
    #endregion

    private void Awake() {
        StringBuilder sb = new StringBuilder();
        sb.Append("Information\n");
        sb.Append("Note that +- values are only for if you upgrade.\n");
        sb.Append("Type: {0}\n");
        sb.Append("Level: {1}\n");
        sb.Append("Damage: {2}\n");
        sb.Append("Health: {3}\n");
        sb.Append("Range: {4}\n");
        sb.Append("Seconds Per Attack: {5}\n");
        sb.Append("Cost to Purchase/Upgrade: ${6}\n");
        _infoTextFormat = sb.ToString();

        _audioSource = GetComponent<AudioSource>();

        foreach (var toggle in PurchaseableItemButtons) {
            _towerToggles.Add(toggle, RespectivePurchasableItemScripts[PurchaseableItemButtons.IndexOf(toggle)]);
        }


        PurchaseButton.onClick.AddListener(PurchaseItem);
        ExitMenuButton.onClick.AddListener(() => GameController.MainInstance.PurchaseMenuCanvas.SetActive(false));
    }

    private void Update() {
        if (_previousIncome != GameController.MainInstance.Money) {
            _previousIncome = GameController.MainInstance.Money;
            CheckAvailableItems();
        }
    }

    public void ClickButton(Toggle button) {
        TowerObject respectiveTower = RespectivePurchasableItemScripts[PurchaseableItemButtons.IndexOf(button)];

        foreach (var item in PurchaseableItemButtons) {
            if (!item.Equals(button)) {
                item.isOn = false;
            }
        }

        if (!button.transform.GetChild(0).GetComponent<Text>().text.Equals(_towerToUpgrade.TowerName)) {
            SetMenuInformation(respectiveTower);
        } else {
            SetMenuInformation(_towerToUpgrade);
        }
    }

    public void OpenPurchaseMenu(TowerObject towerToUpgrade, TowerUIScript respectiveUI) {
        _towerToUpgrade = towerToUpgrade;
        _towerToUpgradeUI = respectiveUI;
        SetMenuInformation(towerToUpgrade);
        CheckAvailableItems();
    }

    private void PurchaseItem() {
        // If the player is downgrading, there should be a small refund (equivalent to the current value - downgrade).
        // This of course means that they lose money if they did a bunch of upgrades. Too bad? :)
        int realTowerValue = _newTowerObject.Value;
        if (_newTowerObject.Value < _towerToUpgrade.Value) {
            realTowerValue = _newTowerObject.Value - _towerToUpgrade.Value;
        }

        // Subtract the value of the tower from the player's money.
        if (GameController.MainInstance.AddMoney(-realTowerValue)) {
            StaticGameMethods.PlaySound(_audioSource, PurchaseSound);
            _towerToUpgrade = _towerToUpgrade.LevelUp(_newTowerObject);
            _towerToUpgradeUI.SetNewTower(_towerToUpgrade);
            SetMenuInformation(_towerToUpgrade);
            CheckAvailableItems();
        }
    }

    private void CheckAvailableItems() {
        foreach (var button in _towerToggles) {
            var currItem = button.Value;
            if ((currItem.TowerName == _towerToUpgrade.TowerName && _towerToUpgrade.Value > GameController.MainInstance.Money) ||
                currItem.Value > GameController.MainInstance.Money) {
                button.Key.GetComponent<Image>().color = new Color(255, 0, 0);
            } else {
                button.Key.GetComponent<Image>().color = new Color(255, 255, 255);
            }
        }
    }


    private void SetMenuInformation(TowerObject selectedTower) {
        if (selectedTower == null) {
            return;
        }

        if (selectedTower.TowerName == _towerToUpgrade.TowerName &&
            selectedTower.TowerName != "NA") {
            PurchaseText.text = "Upgrade";
        } else {
            PurchaseText.text = "Purchase";
        }

        _selectedTower = selectedTower;
        _newTowerObject = selectedTower;
        InformationText.text = string.Format(_infoTextFormat, GetParameters(selectedTower));
    }

    private string[] GetParameters(TowerObject selectedTower) {
        List<string> parameters = new List<string>();

        parameters.Add(selectedTower.TowerName);

        if (selectedTower == _towerToUpgrade && selectedTower.TowerName != "NA") {
            parameters.Add(selectedTower.Level.ToString() + "+ 1");
            parameters.Add(selectedTower.ArrowDamage.ToString() + "+ " + selectedTower.ArrowDamagePerUpgrade.ToString());
            parameters.Add(selectedTower.MaxHealth.ToString() + "+ " + selectedTower.HealthPerUpgrade.ToString());
            parameters.Add(selectedTower.AttackRange.ToString("F2"));
            parameters.Add(selectedTower.SecondsPerAttack.ToString("F2") + "- " + selectedTower.SecondsPerAttackPerUpgrade.ToString("F2"));
        } else {
            parameters.Add(selectedTower.Level.ToString());
            parameters.Add(selectedTower.ArrowDamage.ToString());
            parameters.Add(selectedTower.MaxHealth.ToString());
            parameters.Add(selectedTower.AttackRange.ToString("F2"));
            parameters.Add(selectedTower.SecondsPerAttack.ToString("F2"));
        }

        parameters.Add(selectedTower.Value.ToString());

        return parameters.ToArray();
    }
}
