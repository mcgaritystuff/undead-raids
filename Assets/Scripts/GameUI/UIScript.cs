using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Scripts/GameUI/UIScript")]
public class UIScript : MonoBehaviour {
    public Text EscapesText;
    public Text NewWaveText;
    public Text WalletText;
    public Text WaveNumberText;
    
    void Start() {
        GameController.MainInstance.InitiateEnemyEscapeActions += UpdateEscapesText;
        GameController.MainInstance.CurrencyChanged += UpdateWalletText;
        GameController.MainInstance.InitiateNewWave += UpdateWaveInformation;

        // Call once initially
        UpdateEscapesText();
        UpdateWalletText();
        UpdateWaveInformation();
    }

    private void Update() {
        if (NewWaveText.IsActive()) {
            if (NewWaveText.color.a > 0) {
                NewWaveText.color = new Color(
                    NewWaveText.color.r,
                    NewWaveText.color.g,
                    NewWaveText.color.b,
                    NewWaveText.color.a - .01f
                );
            } else {
                NewWaveText.gameObject.SetActive(false);
            }
        }
    }

    private void UpdateEscapesText() {
        EscapesText.text = $"Escapes: {GameController.MainInstance.EnemiesEscaped}/{GameController.MainInstance.MaxEscapes}";
    }

    private void UpdateWalletText() {
        WalletText.text = $"Wallet: {GameController.MainInstance.Money}";
    }

    private void UpdateWaveInformation() {
        NewWaveText.gameObject.SetActive(true);
        NewWaveText.color = Color.green;
        WaveNumberText.text = $"Wave {GameController.MainInstance.WaveNumber}";
        NewWaveText.text = $"Starting Wave {GameController.MainInstance.WaveNumber}";
    }
}
