using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[AddComponentMenu("Scripts/GameUI/GameOverUIScript")]
public class GameOverUIScript : MonoBehaviour {
    [Header("Text Fields")]
    public string GameOverText = "Game Over!";
    public Text GameOverTextObject;
    public Text TimerTextObject;

    [Header("Canvases")]
    public GameObject MainUICanvas;
    public GameObject PurchaseMenuCanvas;

    [Header("Miscellaneous")]
    public float ReturnToMenuTime = 5.0f;

    private void Update() {
        TimerTextObject.text = $"Returning to the menu in {(ReturnToMenuTime -= Time.deltaTime):F0} seconds.";

        if (ReturnToMenuTime <= 0) {
            SceneManager.LoadScene("Menu");
        }
    }

    private void OnEnable() {
        GameOverTextObject.text = GameOverText;
        MainUICanvas.gameObject.SetActive(false);
        PurchaseMenuCanvas.gameObject.SetActive(false);
    }
}
