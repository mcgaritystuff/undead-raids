using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Scripts/GameUI/TowerUIScript")]
public class TowerUIScript : MonoBehaviour {
    [Header("UI Elements")]
    public Button HealButton;
    public Button UpgradeButton;
    public RectTransform HealthPanelBackground;
    public RectTransform HealthPanelForeground;
    public Text HealthText;
    public Text LevelText;

    [Header("Miscellaneous")]
    public TowerObject RespectiveTowerObject;

    private PurchaseMenu _gamePurchaseMenu;
    private float _currentHealthPercent = 1.0f;

    private void Start() {
        _gamePurchaseMenu = GameController.MainInstance.PurchaseMenuCanvas.GetComponent<PurchaseMenu>();
        UpgradeButton.onClick.AddListener(ClickUpgrade);
        HealButton.onClick.AddListener(ClickHeal);
        SetNewTower(RespectiveTowerObject);
    }

    private void Update() {
        // Upgrade percentage health bar, if needed
        if (RespectiveTowerObject.Level > 0) {
            float healthBarWidthPercent = RespectiveTowerObject.Health / RespectiveTowerObject.MaxHealth;

            if (_currentHealthPercent != healthBarWidthPercent) {
                HealthPanelForeground.sizeDelta = new Vector2(healthBarWidthPercent * HealthPanelBackground.rect.width,
                                                              HealthPanelForeground.sizeDelta.y);
            }

            _currentHealthPercent = healthBarWidthPercent;
        }
    }

    public void SetNewTower(TowerObject newTower) {
        RespectiveTowerObject = newTower;

        // Show the UI information for objects that are at least higher than lvl 0
        if (RespectiveTowerObject.Level > 0) {
            HealthPanelBackground.gameObject.SetActive(true);
            HealthPanelForeground.gameObject.SetActive(true);
            HealthText.gameObject.SetActive(true);
            HealButton.gameObject.SetActive(true);
            LevelText.gameObject.SetActive(true);

            LevelText.text = $"Level: {RespectiveTowerObject.Level}";
        } else {
            HealthPanelBackground.gameObject.SetActive(false);
            HealthPanelForeground.gameObject.SetActive(false);
            HealthText.gameObject.SetActive(false);
            HealButton.gameObject.SetActive(false);
            LevelText.gameObject.SetActive(false);
        }

        HealButton.transform.GetChild(0).GetComponent<Text>().text = $"Heal: {RespectiveTowerObject.HealPrice}";
    }

    private void ClickUpgrade() {
        GameController.MainInstance.PurchaseMenuCanvas.SetActive(true);
        _gamePurchaseMenu.OpenPurchaseMenu(RespectiveTowerObject, this);
    }

    private void ClickHeal() {
        // Don't heal if it's already healed to the max
        if (RespectiveTowerObject.Health == RespectiveTowerObject.MaxHealth) {
            return;
        }

        if (GameController.MainInstance.AddMoney(-RespectiveTowerObject.HealPrice)) {
            RespectiveTowerObject.Heal(RespectiveTowerObject.MaxHealth / 2);
        }
    }
}
