public interface IKillable {
    bool IsDead { get; }

    void Die();
    void TakeDamage(float damage);
}
