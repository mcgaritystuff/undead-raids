﻿using UnityEngine;

interface IPlayerObject : IKillable, IAttacker {
    /// <summary>
    /// To avoid using GetComponent on IPlayerObject objects, this is (assumed to be) a reference to the IPlayerObject's
    /// this.gameObject
    /// </summary>
    GameObject PlayerGameObject { get; }

    /// <summary>
    /// To save on garbage collection, this is (assumed to be) a reference to the IPlayerObject's this.gameObject.transform
    /// </summary>
    Transform PlayerTransform { get; }
}