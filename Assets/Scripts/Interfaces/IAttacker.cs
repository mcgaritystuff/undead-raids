using UnityEngine;

interface IAttacker {
    bool InCombat { get; }

    void AttackTarget(GameObject target);
}