using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[AddComponentMenu("Scripts/Missiles/Arrow")]
public class Arrow : PoolObject {
    [Tooltip("The amount of force to apply to the arrow")]
    public float ArrowForce = 120.0f;

    private bool _hitGroundOnce = false; // To avoid hurting an enemy when touched
    private bool _isPlayerArrow = true; // Keep so we don't keep setting _targetTags to their same values
    private float _damage;
    private float _currentLifeTime;
    private string[] _targetTags;

    private Rigidbody _rigidBody;

    private static readonly float _lifeResetTimer = 5.0f;

    private void Awake() {
        _rigidBody = GetComponent<Rigidbody>();
    }

    private void Update() {
        if ((_currentLifeTime -= Time.deltaTime) <= 0) {
            DisablePoolObject();
        }
    }

    private void OnTriggerEnter(Collider collider) {
        if (_hitGroundOnce) {
            return;
        }

        if (collider.gameObject.CompareTags(_targetTags)) {
            var target = collider.gameObject.GetComponent<IKillable>();

            // Ignore targets who are dead. Hit the next one, or the ground
            if (target.IsDead) {
                return;
            }

            target.TakeDamage(_damage);

            DisablePoolObject();
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Terrain")) {
            _hitGroundOnce = true;
        }
    }

    /// <summary>
    /// Sets the damage and speed of the arrow, and then sets a force to it to shoot at transform.forward
    /// </summary>
    /// <param name="damage">The damage the arrow deals</param>
    /// <param name="speed">How fast it should fly</param>
    /// <param name="isPlayerArrow">To determine whether this should hurt enemies or the player</param>
    public void FireArrow(float damage, float speed, bool isPlayerArrow = true) {
        _damage = damage;
        _rigidBody.AddForce(transform.forward * speed * ArrowForce);

        // Arrow got shot from the pool and doesn't match the target it was for before
        if (_isPlayerArrow != isPlayerArrow || _targetTags == null) {
            if (isPlayerArrow) {
                _targetTags = new string[1] { "Enemy" };
            } else {
                _targetTags = new string[2] { "Player", "Tower" };
            }
        }

        EnablePoolObject(ResetArrowValues);
    }

    /// <summary>
    /// Called to reset when pooled
    /// </summary>
    private void ResetArrowValues() {
        _currentLifeTime = _lifeResetTimer;
        _hitGroundOnce = false;
        _rigidBody.velocity = Vector3.zero;
    }
}
